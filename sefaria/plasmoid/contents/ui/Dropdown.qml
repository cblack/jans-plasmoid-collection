/*
 * Copyright 2020 Carson Black <uhhadd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http: //www.gnu.org/licenses/>.
 */

import QtQuick 2.5
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.1
import QtQuick.XmlListModel 2.0
import org.kde.plasma.components 2.0 as PC2
import org.kde.plasma.components 3.0 as PlasmaComponents
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.kirigami 2.12 as Kirigami

import "api.js" as Requester

PlasmaComponents.Page {
    id: root

    Layout.minimumWidth: units.gridUnit * 14
    Layout.minimumHeight: units.gridUnit * 14
    Layout.preferredWidth: Layout.minimumWidth * 3
    Layout.preferredHeight: Layout.minimumHeight * 1.5

    header: PlasmaExtras.PlasmoidHeading {
        RowLayout {
            Kirigami.Heading {
                text: "Sefaria Calendar"
            }
            Item { Layout.fillWidth: true }
            ToolButton {
                icon.name: "view-refresh"
                onClicked: Requester.fetchCalendar(root)
            }
            width: parent.width
        }
    }

    property bool loading: false
    property var calendar: []
    Component.onCompleted: {
        Requester.fetchCalendar(this)
    }

    BusyIndicator {
        id: busy
        running: root.loading

        width: 64
        height: 64

        anchors.centerIn: parent
    }

    PlasmaExtras.ScrollArea {
        id: scrolly
        anchors.fill: parent
        visible: !root.loading

        ListView {
            id: listy
            model: root.bugs

            anchors.fill: parent

            delegate: Kirigami.Card {
                banner.title: modelData["title"]["en"]
                onClicked: Qt.openUrlExternally(`https://sefaria.org/${modelData["url"]}`)
            }
        }
    }
}