const apiURL = "https://www.sefaria.org/api/calendars"

function fetchCalendar(objectToSetOn) {
    objectToSetOn.loading = true
    let xhr = new XMLHttpRequest()
    xhr.onreadystatechange = () => {
        if (xhr.status == 200) {
            let thing = JSON.parse(xhr.responseText)
            objectToSetOn.calendar = thing["calendar_items"]
            objectToSetOn.loading = false
        }
    }
    xhr.open('GET', apiURL, true)
    xhr.send('')
}
