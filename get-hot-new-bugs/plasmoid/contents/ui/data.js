const bugURL = "https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=REOPENED&f1=creation_ts&f2=bug_status&list_id=1778734&o1=changedafter&o2=anyexact&order=changeddate%20DESC%2Cbug_status%2Cpriority%2Cassigned_to%2Cbug_id&query_format=advanced&v1=24h&ctype=csv"

function csvToJson(csvText) {
    let lines = csvText.split("\n")

    let result = []

    let headers = lines[0].split(",")

    for (let i = 1; i < lines.length; i++) {

        let obj = {}
        let currentline = lines[i].split(",")

        for (let j = 0; j < headers.length; j++) {
            let key = headers[j] ?? ""
            let val = currentline[j] ?? ""

            if (key.startsWith(`"`)) key = key.slice(1)
            if (key.endsWith(`"`)) key = key.slice(0, -1)

            if (val.startsWith(`"`)) val = val.slice(1)
            if (val.endsWith(`"`)) val = val.slice(0, -1)

            if (val === " ---") val = null

            obj[key] = val
        }

        result.push(obj)
    }

    return result
}

function fetchBugs(objectToSetOn) {
    objectToSetOn.loading = true
    let xhr = new XMLHttpRequest()
    xhr.onreadystatechange = () => {
        objectToSetOn.bugs = csvToJson(xhr.responseText)
        objectToSetOn.loading = false
    }
    xhr.open('GET', bugURL, true)
    xhr.send('')
}

function formatDate(date) {
    let parsed = new Date(Date.parse(date + 'T00:00:00.000Z')).getTime()
    return timeSince(parsed)
}

function timeSince(timeStamp) {
    var now = new Date(),
        secondsPast = (now.getTime() - timeStamp) / 1000;
    if (secondsPast < 60) {
        return parseInt(secondsPast) + 's';
    }
    if (secondsPast < 3600) {
        return parseInt(secondsPast / 60) + 'm';
    }
    if (secondsPast <= 86400) {
        return parseInt(secondsPast / 3600) + 'h';
    }
    if (secondsPast > 86400) {
        day = timeStamp.getDate();
        month = timeStamp.toDateString().match(/ [a-zA-Z]*/)[0].replace(" ", "");
        year = timeStamp.getFullYear() == now.getFullYear() ? "" : " " + timeStamp.getFullYear();
        return day + " " + month + year;
    }
}